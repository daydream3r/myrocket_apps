<?php 

use App\Models\Menu;
use App\Models\User;
use App\Models\Partner;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;

function formatRupiah($angka){
	return 'Rp. '.number_format($angka,2,',','.');
}
function getMenus(){	
	return Menu::with('children')->whereNull('parent_id')->orderBy('position')->get();
}

function getPermissionsId($user){
	$arrayIds = [];
	foreach($user->getPermissionsViaRoles() as $permission){
		array_push($arrayIds,intval($permission->id));
	}
	return $arrayIds;
}

function deleteRole($role){
	DB::table('role_has_permissions')->where('role_id',$role)->delete();
	$role->delete();
}

function revokeAllPermission($permission){		
	foreach(Role::all() as $role){
		$e = $role->revokePermissionTo($permission);
		$permission->removeRole($role);
	}
	$permission->delete();
}

function role_permission($role,$permission){
	$id = DB::table('role_has_permissions')->where('role_id',$role)->where('permission_id',$permission->id)->first();
	return ($id?$id->permission_id:null);
}

function status($status){
	switch ($status) {
		case 1:
			return 'active';
			break;
		
		default:
			return 'non active';
			break;
	}
}

function known(){
	return [
		'koran'=>'Koran',
		'web'=>'Website MyRoket',
		'social-media'=>'Social Media',
		'info-teman'=>'Info Teman',
		'lainnya'=>'Lainnya'
	];
}

function findRole($id){
	return DB::table('model_has_roles')->where('model_id',$id)->first();
}

function getPICName($id){
	return (User::find($id))?User::find($id)->name:'-';
}

function roleDependency(User $user){
	$query = '';
	$hubRole = Role::whereName('HUB akses')->first()->id;	
	$posRole = Role::whereName('POS akses')->first()->id;
	
	if($user->hasRole('HUB akses')){
		$query = User::whereIn('id',function($query) use ($hubRole){
			$query->select('user_id')->from('user_profiles')->whereNotNull('partner_id')->whereIn('partner_id',Partner::all()->pluck('id'));
		})->get();
	}else if($user->hasRole('POS akses')){
		$query = User::whereIn('id',function($query) use ($hubRole){
			$query->select('user_id')->from('user_profiles')->whereNotNull('partner_id')->whereIn('partner_id',Partner::whereNotIn('name',['hub'])->pluck('id'));
		})->get();
	}else{				
		$query = User::where('id',function($query) use ($hubRole){
			$query->select('user_id')->from('user_profiles')->whereNotNull('partner_id')->where('partner_id',auth()->user()->profile->partner_id)->limit(1);
		})->get();
	}
	return $query->pluck('id');
}
