<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function permission()
    {
        return $this->belongsTo(Permission::class, 'permission_id', 'id');
    }


    public function parent()
    {
        return $this->belongsTo(Menu::class, 'parent_id')->whereNull('parent_id')->with('parent');
    }


    public function children()
    {        
        return $this->hasMany(Menu::class, 'parent_id')->with('children')->orderBy('position');
    }

}
