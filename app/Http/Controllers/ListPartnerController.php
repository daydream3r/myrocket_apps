<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Models\Partner;
use App\Models\User;
use App\Models\UserProfile;
use Livewire\Component;
use DB;

class ListPartnerController extends Component
{
    public $user_id;
    public $pic;
    public $partner_id;
    public $list_partner;
    public $assignTo;
    public $area;
    public $name;
    public $kecamatan;
    public $kelurahan;
    public $title = 'List Partner';

    public $update = false;

    public function mount(){
        if (!Gate::allows('view partner')) {            
            abort(403);
        }

        $this->list_partner=Partner::all();         

    }

    public function render(){   

        $partners =  DB::table('users')
            ->select('users.id','users.name','users.email','user_profiles.phone_number','user_profiles.report_to','user_profiles.area','partners.name as partner_name','user_profiles.kecamatan','user_profiles.kelurahan')
            ->join('user_profiles','users.id', '=', 'user_profiles.user_id')
            ->join('partners','partners.id', '=', 'user_profiles.partner_id')
            ->whereNotNull('user_profiles.partner_id');              
        $non_partner =  User::whereIn('id',function($query){
            $query->select('user_id')->from('user_profiles')->whereNull('partner_id');
        })->get();      
        if(!auth()->user()->hasRole('admin')){  
                $partners->whereIn('users.id',roleDependency(auth()->user()))->where('user_profiles.area','like','%'.auth()->user()->profile->area.'%');   
        }
        $partners = $partners->orderBy('users.created_at')->get();
        
        return view('pages.list-partner',[
            'partners'=>$partners,                        
            'users'=>$non_partner,                        
            'principles'=>User::all(),                        
        ])
        ->layout('layout.app-layout',['title'=>'Myroket App | '.$this->title]);
    }


    public function store()
    {        
        
        $this->_validate();

        $data = [
            'partner_id'  => $this->partner_id,            
            'area'  => $this->area,            
            'kecamatan'  => $this->kecamatan,            
            'kelurahan'  => $this->kelurahan, 
            'report_to'  => $this->assignTo,           
        ];

        UserProfile::where('user_id',$this->pic)->update($data);

        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Disimpan']);
    }

    /**
     * Update data
     */
    public function update()
    {
        $rule = [            
            'assignTo'  => 'required',
            'partner_id'  => 'required',
            'area'  => 'required',
            'kecamatan'  => 'required',
            'kelurahan'  => 'required',
        ];

        $this->validate($rule);

        $data = [
            'report_to'  => $this->assignTo,
            'partner_id'  => $this->partner_id,
            'area'  => $this->area,
            'kecamatan'  => $this->kecamatan,            
            'kelurahan'  => $this->kelurahan, 
        ];

        $row = UserProfile::where('user_id',$this->user_id)->first();

        $row->update($data);

        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Diupdate']);
    }

    /**
     * Delete data
     */
    public function delete()
    {
        $partner = User::find($this->partner_id);
        $partner->profile()->delete();       
        $partner->delete();       
        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Dihapus']);
        
    }

    public function _validate()
    {
        $rule = [            
            'assignTo'  => 'required',
            'partner_id'  => 'required',
            'area'  => 'required',
            'pic'  => 'required',
            'kecamatan'  => 'required',
            'kelurahan'  => 'required',
        ];

        return $this->validate($rule);
    }

    public function getDataById($id)
    {           
        $partner = UserProfile::where('user_id',$id)->first();        
        $this->user_id = $id;
        $this->partner_id = $partner->partner_id;
        $this->area = $partner->area;
        $this->assignTo = $partner->report_to;
        $this->kecamatan = $partner->kecamatan;
        $this->kelurahan = $partner->kelurahan;

        $this->update = true;
        $this->emit('showModal');
    }

    public function getId($id)
    {
        $partner = User::find($id);
        $this->partner_id = $partner->id;
        $this->emit('showModalConfirm');
    }

    public function showModal()
    {
        $this->emit('showModal');
    }

    
    public function _reset()
    {                
        $this->emit('closeModal');
        $this->name = null;
        $this->pic = null;
        $this->partner_id = null;
        $this->users = null;
        $this->assignTo = null;
        $this->area = null;
        $this->kecamatan = null;
        $this->kelurahan = null;

        $this->update = false;
        $this->resetErrorBag();
        $this->resetValidation();        
    }



}
