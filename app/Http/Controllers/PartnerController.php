<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Partner;
use Livewire\Component;
use Illuminate\Support\Facades\Gate;

class PartnerController extends Component
{
    public $partner_id;
    public $name;
    public $parent;
    public $parent_id;
    public $title = 'Partner';

    public $update = false;

    public function mount(){
        if (!Gate::allows('view partner')) {            
            abort(403);
        }
        $this->parent = Partner::all();
    }

    public function render(){
        return view('pages.partner',['partners'=>partner::all()])->layout('layout.app-layout',['title'=>'Myroket App | '.$this->title]);
    }

    
    public function store()
    {        
        $this->_validate();

        $data = [
            'name'  => $this->name,            
        ];

        partner::create($data);

        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Disimpan']);
    }

    /**
     * Update data
     */
    public function update()
    {
        $this->_validate();

        $data = [
            'name'  => $this->name,
            'parent_id'  => $this->parent_id,
        ];

        $row = Partner::find($this->partner_id);

        $row->update($data);

        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Diupdate']);
    }

    /**
     * Delete data
     */
    public function delete()
    {
        $partner = Partner::find($this->partner_id);
        $partner->delete();       
        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Dihapus']);
        
    }

    public function _validate()
    {
        $rule = [
            'name'  => 'required',
        ];

        return $this->validate($rule);
    }

    public function getDataById($id)
    {
        $partner = Partner::find($id);
        $this->parent = Partner::where('id','<>',$id)->get();
        $this->partner_id = $id;
        $this->parent_id = $partner->parent_id;        
        $this->name = $partner->name;

        $this->update = true;
        $this->emit('showModal');
    }

    public function getId($id)
    {
        $partner = Partner::find($id);
        $this->partner_id = $partner->id;
        $this->emit('showModalConfirm');
    }

    public function showModal()
    {
        $this->emit('showModal');
    }

    
    public function _reset()
    {                
        $this->emit('closeModal');
        $this->name = null;
        $this->partner_id = null;
        $this->parent_id = null;
        $this->update = false;        
    }



}
