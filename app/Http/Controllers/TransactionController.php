<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Livewire\Component;

class TransactionController extends Component
{
    public function cepat(){
        if (!Gate::allows('view transaction')) {            
            abort(403);
        }
        return view('pages.transaction.roket-cepat');
    }

    public function pindahan(){
        if (!Gate::allows('view transaction')) {            
            abort(403);
        }
        return view('pages.transaction.roket-pindahan');
    }
    public function esok(){
        if (!Gate::allows('view transaction')) {            
            abort(403);
        }
        return view('pages.transaction.roket-esok');
    }
    public function darat(){
        if (!Gate::allows('view transaction')) {            
            abort(403);
        }
        return view('pages.transaction.roket-darat');
    }
    public function regular(){
        if (!Gate::allows('view transaction')) {            
            abort(403);
        }
        return view('pages.transaction.roket-regular');
    }
    public function kilat(){
        if (!Gate::allows('view transaction')) {            
            abort(403);
        }
        return view('pages.transaction.roket-kilat');
    }
    public function plus(){
        if (!Gate::allows('view transaction')) {            
            abort(403);
        }
        return view('pages.transaction.roket-kilat');
    }
}
