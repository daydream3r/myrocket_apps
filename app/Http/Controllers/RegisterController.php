<?php

namespace App\Http\Controllers;


use App\Models\User;
use App\Models\Partner;

use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Mail;
use DB;

class RegisterController extends Component
{
    use WithFileUploads;

    public $title = 'Register New Membership';
    public $name;
    public $password;
    public $password_confirmation;
    public $email;
    public $phone_number;
    public $mitra;
    public $mitra_id;
    public $address;
    public $kelurahan;
    public $kecamatan;
    public $provinsi;
    public $kode_pos;
    public $gmap;
    public $profile;
    public $ktp;
    public $kk;
    public $sim;
    public $skck;
    public $domisili;
    public $known;

    public function render(){
        return view('auth.register',[
            'partners'=>Partner::all()
        ])
        ->layout('layout.auth-layout',['title'=>'Myroket App | '.$this->title]);        
    }

    public function _validate()
    {
        $rules = [
            'name'  => 'required',         
            'email'  => 'required|unique:users,email',         
            'phone_number'  => 'required',                                                                             
            'address'  => 'required',                         
            'kelurahan'  => 'required',                         
            'kecamatan'  => 'required',                         
            'provinsi'  => 'required',                         
            'kode_pos'  => 'required',                         
            'profile'  => 'required|file|mimes:jpg,png|max:500',                         
            'kk'  => 'required|file|mimes:jpg,png|max:500',                         
            'ktp'  => 'required|file|mimes:jpg,png|max:500',                         
            'sim'  => 'required|file|mimes:jpg,png|max:500',                         
            'skck'  => 'required|file|mimes:jpg,png|max:500',                         
            'domisili'  => 'required|file|mimes:jpg,png|max:500',                         
            'known'  => 'required',                                 
            'password'  => 'required|confirmed',                                             
        ];

        return $this->validate($rules);
    }

    public function store(){
        $this->_validate();

        $data = [
            'name'  => $this->name,
            'email'  => $this->email,
            'password'  => bcrypt($this->password),
            'status'=>1,
        ];


        try {
            DB::beginTransaction();
            $user = User::create($data);
        if($user){
            $profile = [
        'phone_number'=>$this->phone_number,
                'address'=>$this->address,
                'partner_id'=>$this->mitra_id?$this->mitra_id:null,
                'partner_name'=>$this->mitra,                
                'kode_pos'=>$this->kode_pos,
                'kelurahan'=>$this->kelurahan,
                'kecamatan'=>$this->kecamatan,
                'provinsi'=>$this->provinsi,              
                'gmaps'=>$this->gmap,
                'from_known'=>$this->known,
                'avatar'=>$this->profile->store('user/profile','public'),
                'kk'=>$this->profile->store('user/kk','public'),
                'ktp'=>$this->profile->store('user/ktp','public'),
                'skck'=>$this->profile->store('user/skck','public'),
                'domisili'=>$this->profile->store('user/domisili','public'),
                'sim'=>$this->profile->store('user/sim','public'),
                'user_id'=>$user->id,                
                'created_by'=>$user->id];
        
            $user->profile()->create($profile);
            $user->assignRole(2);
            $email = $this->email;

            Mail::send('email.new-registration', ['user' => $user], function ($message) use ($email) {
            $message->from('no-reply@myroket.com');
            $message->to($email);
            $message->subject('New Registration Membership');
            });

            Mail::send('email.admin-new-registration', ['user' => $user], function ($message){
            $message->from('no-reply@myroket.com');
            $message->to('support@myroket.com');
            $message->subject('Admin Registration Notification');
            });
            DB::commit();
            $this->_reset();
        }
            
        } catch (\Exception $e) {  
        dd($e->getMessage());          
            DB::rollBack();
            $this->addError('valid','Something Wrong!Please Try again');
        }

        
    }

    public function _reset(){
        session()->flash('success', 'Your Registration Success,Check email for detail.');
        return redirect()->route('auth.login');

    }
}
