<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PasswordController extends Controller
{
    public function index(){
        return view('pages.change-password');
    }

    public function store(Request $request){
        $validation = [
            'password'=>'required|confirmed',
        ];

        $request->validate($validation);
            auth()->user()->update([
            'password'=>bcrypt($request->password)
        ]);    
        return redirect('change-password')->with(['success'=>'Your Password has been updated']);
    }
}
