<?php

namespace App\Http\Controllers;


use App\Models\User;
use App\Models\Partner;

use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Mail;
use DB;

class ProfileController extends Component
{
    use WithFileUploads;

    public $title = 'Profile';
    public $name;
    public $password;
    public $password_confirmation;
    public $email;
    public $phone_number;
    public $mitra;
    public $mitra_id;
    public $address;
    public $kelurahan;
    public $kecamatan;
    public $provinsi;
    public $kode_pos;
    public $gmap;
    public $profile;
    public $profile_picture;
    public $ktp;
    public $kk;
    public $sim;
    public $skck;
    public $domisili;
    public $known;
    

    public function render(){
        return view('pages.profile',[
            'user'=>auth()->user(),
            'partners'=>Partner::all(),            
        ])
        ->layout('layout.app-layout',['title'=>'Myroket App | '.$this->title]);        
    }

    public function mount(){
        $user = auth()->user();
        $this->name = $user->name;
        $this->email = $user->email;
        $this->phone_number = $user->profile->phone_number??'';
        $this->mitra = $user->profile->partner_name??'';
        $this->mitra_id = $user->profile->partner_id??'';
        $this->profile_picture = $user->profile->avatar??'';
        $this->address = $user->profile->address??'';
        $this->gmap = $user->profile->gmaps??'';
        $this->kelurahan = $user->profile->kelurahan??'';
        $this->kecamatan= $user->profile->kecamatan??'';
        $this->provinsi= $user->profile->provinsi??'';
        $this->kode_pos= $user->profile->kode_pos??'';
    }

    public function _validate()
    {
        $rules = [
            'name'  => 'required',                         
            'phone_number'  => 'required',         
            'mitra'  => 'required_with:mitra_id',                                                             
            'address'  => 'required',                         
            'kelurahan'  => 'required',                         
            'kecamatan'  => 'required',                         
            'provinsi'  => 'required',                         
            'kode_pos'  => 'required',                                                                     
        ];

        return $this->validate($rules);
    }

    public function store(){
        
        $this->_validate();

        $data = [
            'name'  => $this->name,
        ];
        try {
            DB::beginTransaction();
            $users = auth()->user();
            $user = $users->update($data);
            $profile = [
                'phone_number'=>$this->phone_number,
                'address'=>$this->address,
                'partner_id'=>$this->mitra_id?$this->mitra_id:null,
                'partner_name'=>$this->mitra,                
                'kode_pos'=>$this->kode_pos,
                'kelurahan'=>$this->kelurahan,
                'kecamatan'=>$this->kecamatan,
                'provinsi'=>$this->provinsi,
                'gmaps'=>$this->gmap,                
            ];
            if($this->profile){
                $profile['avatar'] = $this->profile->store('user/profile','public');
            } 
            if($user){
            if($users->profile){ 
                          
            $users->profile->update($profile);
            }else{
                $profile['user_id']=$users->id;
                $profile['created_by']=1;

                $users->profile()->create($profile);
            }
            
            DB::commit();
        }
            
        } catch (\Exception $e) {    
            dd($e->getMessage());        
            DB::rollBack();
            $this->addError('valid','Something Wrong!Please Try again');
        }

        $this->_reset();
    }

    public function _reset(){
        session()->flash('success', 'Your Profile has been Updated.');
        return redirect()->route('profile');

    }
}
