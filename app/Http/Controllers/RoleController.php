<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Gate;

use Livewire\Component;

class RoleController extends Component
{
    public $role_id;
    public $name;
    public $permissions;
    public $permission_ids = [];
        
    public $title = 'Role';
    public $update = false;
    public $hasAssign = false;
    public function mount(){
        if (!Gate::allows('view role')) {            
            abort(403);
        }
    }

    public function render(){
        return view('pages.role',[
            'roles'=>Role::all(),            
        ])
        ->layout('layout.app-layout',['title'=>'Myroket App | '.$this->title]);
    }

    public function assign(){
        Role::find($this->role_id)->syncPermissions($this->permission_ids);
        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Disimpan']);
    }

    public function getRoleId($id)
    {        
        $role = Role::find($id);        
        $permissions = Permission::with('roles')->whereHas('roles', function ($query) use ($id) {
            return $query->where('roles.id', $id);
        })->get();
        foreach ($permissions as $permission) {
            $this->permission_ids[] = "$permission->id";
        }
        $this->role_id = $role->id;
        $this->hasAssign = true;
        $this->permissions = Permission::with('roles')->orderByDesc('created_at')->get();
        $this->emit('showModal');
    }

    public function _validate()
    {
        $rule = [
            'name'  => 'required',         
        ];

        return $this->validate($rule);
    }

    public function store()
    {        
        $this->_validate();

        $data = [
            'name'  => $this->name,
        ];

        Role::create($data);

        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Disimpan']);
    }

    public function update()
    {

        $this->_validate();

        $data = [
            'name'  => $this->name,
        ];

        $row = Role::find($this->role_id);

        $row->update($data);

        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Diupdate']);
    }

    public function getDataById($id)
    {
        $role = Role::find($id);
        $this->role_id = $role->id;
        $this->name = $role->name;       

        $this->update = true;
        $this->emit('showModal');
    }

    public function getId($id)
    {
        $role = Role::find($id);
        $this->role_id = $role->id;
        $this->emit('showModalConfirm');
    }

    public function delete()
    {
        $role = Role::find($this->role_id);
        deleteRole($role);           
        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Dihapus']);
        
    }

    public function showModal()
    {
        $this->emit('showModal');
    }

    
    public function _reset()
    {                
        $this->emit('closeModal');
        $this->name = null;
        $this->permission_ids = [];

        $this->update = false;        
        $this->hasAssign = false;        
    }
}
