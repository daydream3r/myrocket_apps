<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Str;
use DB;
use Carbon\Carbon;

class LoginController extends Controller
{
    public function __construct(){
        $this->middleware('guest')->except('logout');
    }
    //
    public function index(){
        return view('auth.login');
    }

    public function store(Request $request){
        $validation = [
            'email'=>'required',
            'password'=>'required'
        ];

        $request->validate($validation);


        $user = User::whereEmail($request->email)->first();            
        if($user){
            if(Hash::check($request->password,$user->password)){
                if(!$user->email_verified_at && !$user->verified_by){
                    return back()->withInput()->with([
                        'error'=>'Your must be verified to login'
                    ]);
                } 
                Auth::login($user);
                auth()->user()->update([
                    'last_login'=>Carbon::now()
                ]);
                $request->session()->regenerate();
                return redirect('dashboard');
            }

            return back()->withInput()->with([
                'error'=>'Your enter email and password doesn\'t match'
            ]);
        }

        return back()->withInput()->with([
                'error'=>'Your enter email not found'
            ]);
            
    }

    public function forgot(){
        return view('auth.forgot-password');
    }
    public function new(Request $request){
        $row = DB::table('password_resets')->where(['token' => $request->token])->first();

        if (!$row) {
            return redirect()->route('auth.login')->with('error', 'Invalid Token');
        }

        if (Auth::check()) {
            return redirect()->intended('dashboard');
        }
        return view('auth.new-password');

    }
    public function forgot_save(Request $request){
        $validation = [
            'email'=>'required',
        ];
        $request->validate($validation);
        $token = Str::random(128);
        $user = User::whereEmail($request->email)->first();
        if(!$user){
            return back()->withInput()->with([
                'error'=>'Your enter email not found'
            ]);
        }        
        $url = route('auth.new',['token'=>$token,'email'=>$request->email]);



         $mail = Mail::send('email.forgot-password', ['user' => $user,'action'=>$url], function ($message) use ($request) {
            $message->from('no-reply@myroket.com');
            $message->to($request->email);
            $message->subject('Forgot Password Notification');
            });

         DB::table('password_resets')->insert([
            'token'=>$token,
            'email'=>$request->email,
            'created_at'=>now()
         ]);

         return redirect()->route('auth.login')->with([
                'success'=>'Check your mail for create new password'
            ]);


    }
    public function new_save(Request $request){
        $validation = [
            'password'=>'required|confirmed',
        ];
        $request->validate($validation);

        $row = DB::table('password_resets')->where(['token' => $request->token])->first();
        $today = Carbon::now();
        $user = User::where('email', $row->email)->first();

        if ( $today->diffInMinutes($row->created_at) >= 60) {
            return redirect()->route('auth.login')->with('error', 'Invalid Token');
        }

        if (!$row) {
            return redirect()->route('auth.login')->with('error', 'Invalid Token');
        }        

        DB::table('password_resets')->where(['email' => $user->email])->delete();

        $user->update(['password' => bcrypt($request->password)]);
        return redirect()->route('auth.login')->with('success', 'Password successfuly updated');

    }

    public function logout(Request $request){
        Auth::logout();

    $request->session()->invalidate();

    $request->session()->regenerateToken();

    return redirect('/');
    
    }
}
