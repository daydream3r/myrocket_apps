<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Livewire\Component;
use Illuminate\Support\Facades\Gate;

class PermissionController extends Component
{
    public $permission_id;
    public $name;
    public $title = 'Permission';



    public $update = false;
    public $confirm = false;

    public function mount(){
        if (!Gate::allows('view permission')) {            
            abort(403);
        }
    }

    public function render(){
        return view('pages.permission',['permissions'=>Permission::all()])->layout('layout.app-layout',['title'=>'Myroket App | '.$this->title]);
    }

    
    public function store()
    {        
        $this->_validate();

        $data = [
            'name'  => $this->name,            
        ];

        Permission::create($data);

        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Disimpan']);
    }

    /**
     * Update data
     */
    public function update()
    {
        $this->_validate();

        $data = [
            'name'  => $this->name,
        ];

        $row = Permission::find($this->permission_id);

        $row->update($data);

        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Diupdate']);
    }

    /**
     * Delete data
     */
    public function delete()
    {
        $permission = Permission::find($this->permission_id);
        revokeAllPermission($permission);        
        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Dihapus']);
        
    }

    public function _validate()
    {
        $rule = [
            'name'  => 'required',
        ];

        return $this->validate($rule);
    }

    public function getDataById($id)
    {
        $permission = Permission::find($id);
        $this->permission_id = $id;
        $this->name = $permission->name;
        $this->update = true;
        $this->emit('showModal');
    }

    public function getId($id)
    {
        $permission = Permission::find($id);
        $this->permission_id = $permission->id;
        $this->emit('showModalConfirm');
    }

    public function showModal()
    {
        $this->emit('showModal');
    }

    
    public function _reset()
    {                
        $this->emit('closeModal');
        $this->name = null;
        $this->permission_id = null;
        $this->update = false;        
    }



}
