<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Gate;
use Str;
use DB;

use Livewire\Component;

class UserController extends Component
{
    public $user_id;
    public $role_id;
    public $name;
    public $email;
    public $password;
    public $phone_number;
    public $status;
        
    public $title = 'User';

    public $verify = false;
    public $update = false;

    public function mount(){
        if (!Gate::allows('view user')) {            
            abort(403);
        }
    }

    public function render(){
        return view('pages.user',[
            'users'=>User::all(),            
            'roles'=>Role::where('name','<>','admin')->get(),            
        ])
        ->layout('layout.app-layout',['title'=>'Myroket App | '.$this->title]);
    }

    public function _validate()
    {
        $rule = [
            'name'  => 'required',                        
            'role_id'=>'required'
        ];

        if($this->update == false){
            $rule['email'] = 'required';
        }

        return $this->validate($rule);
    }

    public function verifyNow(){
        User::find($this->user_id)->update([
            'email_verified_at'=>now(),
            'verified_by'=>auth()->user()->id
        ]);
        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'User Berhasil Diverifikasi']);
        
    }

    public function verify($id){
        $this->user_id = $id;

        $this->emit('showModalVerify');
    }

    public function store()
    {        
        $this->_validate();

        $password = Str::random(10);

        $data = [
            'name'  => $this->name,
            'email' => $this->email,
            'password'=>bcrypt($password),
        ];

        try {
            DB::beginTransaction();
            $user = User::create($data);
        if($user){
            $user->profile()->create([
                'phone_number'=>$this->phone_number,
                'user_id'=>$user->id,
                'created_by'=>auth()->user()->id,
            ]);
            $user->assignRole([$this->role_id??2]);
            $email = $this->email;

            $mail = Mail::send('email.register-user', ['user' => $user,'password'=>$password], function ($message) use ($email) {
            $message->from('no-reply@myroket.com');
            $message->to($email);
            $message->subject('New Member Notification');
            });
            DB::commit();
        }
            
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e->getMessage());
            $this->addError('valid','Something Wrong!Please Try again');
        }
        
        


        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Disimpan']);
    }

    /**
     * Update data
     */
    public function update()
    {
        $this->_validate();        

        $data = [
            'name'  => $this->name,                                                           
            'status'  => $this->status,                        
        ];

        $row = User::find($this->user_id);
        
        $row->update($data);
        if($row->profile){
            $row->profile()->update([
            'phone_number'  => $this->phone_number
        ]);    
        }else{
            $row->profile()->create([
            'user_id'=>$this->user_id,
            'created_by'=>auth()->user()->id,
            'phone_number'  => $this->phone_number
            ]);
        }

        $role = DB::table('model_has_roles')->where('model_id',$this->user_id);        
        if($role->count()){
            $role->update([
            'role_id'=>$this->role_id
        ]);    
        }else{            
            $row->assignRole($this->role_id);
        }
        

        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Diupdate']);
    }

    public function getDataById($id)
    {

        $user = User::find($id); 
        $role = findRole($user->id);                            
        $this->user_id = $user->id;
        $this->name = $user->name;
        $this->email = $user->email;
        $this->role_id = ($role)?$role->role_id:'';
        $this->status = $user->status;
        $this->phone_number = isset($user->profile->phone_number)?$user->profile->phone_number:'';        

        $this->update = true;
        $this->emit('showModal');
    }

    public function getId($id)
    {
        $user = User::find($id);
        $this->user_id = $user->id;
        $this->emit('showModalConfirm');
    }

    public function delete()
    {
        $user = User::find($this->user_id);
        $user->delete();            
        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Dihapus']);
        
    }


    public function showModal()
    {
        $this->emit('showModal');
    }

    
    public function _reset()
    {                
        $this->emit('closeModal');
        $this->name = null;
        $this->phone_number = null;
        $this->email = null;        
        $this->role_id = null;        
        $this->status = null;        
                
        $this->update = false;        
        $this->verify = false;        
    }


}
