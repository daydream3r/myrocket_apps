<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
use App\Models\Permission;
use Illuminate\Support\Facades\Gate;

use Livewire\Component;

class MenuController extends Component
{
    public $menu_id;
    public $permission_id;
    public $name;
    public $slug;
    public $parent_id;    
    public $position;    
    public $title = 'Menu';

    public $update = false;

    public function mount(){
        if (!Gate::allows('view menu')) {            
            abort(403);
        }
    }

    public function render(){
        return view('pages.menu',[
            'menus'=>Menu::all(),            
            'parents'=>Menu::whereNull('parent_id')->get(),            
            'permissions'=>Permission::where('name','like','%view%')->get(),
        ])
        ->layout('layout.app-layout',['title'=>'Myroket App | '.$this->title]);
    }

    public function _validate()
    {
        $rule = [
            'name'  => 'required',            
            'permission_id'  => 'required',
        ];

        return $this->validate($rule);
    }

    public function store()
    {        
        $this->_validate();

        $data = [
            'name'  => $this->name,
            'permission_id'  => $this->permission_id,
            'slug'  => $this->slug??'#',
            'parent_id'  => $this->parent_id?$this->parent_id:null,
            'position'=>$this->position??0,
        ];

        Menu::create($data);

        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Disimpan']);
    }

    /**
     * Update data
     */
    public function update()
    {

        $this->_validate();

        $data = [
            'name'  => $this->name,
            'permission_id'  => $this->permission_id,
            'slug'  => $this->slug??'#',
            'parent_id'  => $this->parent_id?$this->parent_id:null,
            'position'=>$this->position??0,
        ];

        $row = Menu::find($this->menu_id);

        $row->update($data);

        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Diupdate']);
    }

    public function getDataById($id)
    {
        $menu = Menu::find($id);
        $this->menu_id = $menu->id;
        $this->name = $menu->name;
        $this->slug = $menu->slug;        
        $this->permission_id = $menu->permission_id;        
        $this->parent_id = $menu->parent_id;        
        $this->position = $menu->position;        

        $this->update = true;
        $this->emit('showModal');
    }

    public function getId($id)
    {
        $menu = Menu::find($id);
        $this->menu_id = $menu->id;
        $this->emit('showModalConfirm');
    }

    public function delete()
    {
        $menu = Menu::find($this->menu_id);
        $menu->delete();            
        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Dihapus']);
        
    }


    public function showModal()
    {
        $this->emit('showModal');
    }

    
    public function _reset()
    {                
        $this->emit('closeModal');
        $this->name = null;
        $this->slug = null;
        $this->parent_id = null;
        $this->position = null;
        $this->permission_id = null;
        $this->menu_id = null;

        $this->update = false;        
    }

    

}
