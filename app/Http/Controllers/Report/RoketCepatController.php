<?php

namespace App\Http\Controllers\Report;


use App\Models\Partner;
use App\Models\User;
use App\Models\UserProfile;
use Livewire\Component;
use Illuminate\Support\Facades\Gate;
use DB;

class RoketCepatController extends Component
{
    public $no_order;
    public $pembayaran;
    public $total_produk;
    public $total_jasa;
    public $total_kurir;
    public $total_tambahan;
    public $sub_total;
    public $total;
    public $nama_mitra;
    public $mitra;
    public $driver;
    public $nama_driver;
    public $asal;
    public $tujuan;
    public $diskon;
    public $kode_voucher;
    public $status;


    public $report_id;        
    public $title = 'Roket Cepat';

    public $update = false;

    public function mount(){
        if (!Gate::allows('view report')) {            
            abort(403);
        }
    }

    public function render(){        
        $reports =  DB::table('roket_cepat');  
        $kecamatan = auth()->user()->profile->kecamatan;                   
        $kelurahan = auth()->user()->profile->kelurahan; 
        $pos = $kecamatan.' '.$kelurahan;

        if(!auth()->user()->hasRole('admin')){            
            if(auth()->user()->hasRole('HUB akses')){
                $reports->where('nama_mitra','like','%'.$kecamatan.'%');
            }else{
                $reports->where('nama_mitra','like','%'.$pos.'%');
            }
        }
        $reports = $reports->get();
        
        return view('pages.report.roket-cepat',[
            'reports'=>$reports,            
        ])
        ->layout('layout.app-layout',['title'=>'Myroket App | '.$this->title]);
    }

    /**
     * Update data
     */
    public function update()
    {        

        $row = DB::table('roket_cepat')->whereId($this->report_id);    


        $data = [
            'no_order' => $this->no_order,
            'pembayaran' => $this->pembayaran,
            'total_produk' => $this->total_produk,
            'total_jasa' => $this->total_jasa,
            'total_kurir' => $this->total_kurir,
            'total_tambahan' => $this->total_tambahan,
            'sub_total' => $this->sub_total,
            'total' => $this->total,
            'nama_mitra' => $this->nama_mitra,
            'mitra' => $this->mitra,
            'driver' => $this->driver,
            'nama_driver' => $this->nama_driver,
            'asal' => $this->asal,
            'tujuan' => $this->tujuan,
            'diskon' => $this->diskon,
            'kode_voucher' => $this->kode_voucher,
            'status' => $this->status
        ];

        $row->update($data);

        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Diupdate']);
    }

    /**
     * Delete data
     */
    public function delete()
    {
        $report = DB::table('roket_cepat')->whereId($this->report_id);            
        $report->delete();       
        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Dihapus']);
        
    }    

    public function getDataById($id)
    {        
        $report = DB::table('roket_cepat')->find($id);   
        $this->report_id = $id;
        $this->no_order = $report->no_order;
        $this->pembayaran = $report->pembayaran;
        $this->total_produk = $report->total_produk;
        $this->total_jasa = $report->total_jasa;
        $this->total_kurir = $report->total_kurir;
        $this->total_tambahan = $report->total_tambahan;
        $this->sub_total = $report->sub_total;
        $this->total = $report->total;
        $this->nama_mitra = $report->nama_mitra;
        $this->mitra = $report->mitra;
        $this->driver = $report->driver;
        $this->nama_driver = $report->nama_driver;
        $this->asal = $report->asal;
        $this->tujuan = $report->tujuan;
        $this->diskon = $report->diskon;
        $this->kode_voucher = $report->kode_voucher;
        $this->status = $report->status;

        $this->update = true;
        $this->emit('showModal');
    }

    public function getId($id)
    {
        $report = DB::table('roket_cepat')->find($id);        
        $this->report_id = $report->id;
        $this->emit('showModalConfirm');
    }

    public function showModal()
    {
        $this->emit('showModal');
    }

    
    public function _reset()
    {                
        $this->emit('closeModal');        
        $this->report_id = null;
        $this->no_order = null;
        $this->pembayaran = null;
        $this->total_produk = null;
        $this->total_jasa = null;
        $this->total_kurir = null;
        $this->total_tambahan = null;
        $this->sub_total = null;
        $this->total = null;
        $this->nama_mitra = null;
        $this->mitra = null;
        $this->driver = null;
        $this->nama_driver = null;
        $this->asal = null;
        $this->tujuan = null;
        $this->diskon = null;
        $this->kode_voucher = null;
        $this->status = null;

        $this->update = false;        
    }



}
