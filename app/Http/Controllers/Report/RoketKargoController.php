<?php

namespace App\Http\Controllers\Report;


use App\Models\Partner;
use App\Models\User;
use App\Models\UserProfile;
use Livewire\Component;
use Illuminate\Support\Facades\Gate;
use DB;

class RoketKargoController extends Component
{
    public $no_order;
    public $nama_pengirim;
    public $asal_kiriman;
    public $tujuan_kiriman;
    public $mitra_myroket;



    public $report_id;        
    public $title = 'Roket Cepat';

    public $update = false;

    public function mount(){
        if (!Gate::allows('view report')) {            
            abort(403);
        }
    }

    public function render(){       
        $reports =  DB::table('roket_kargo');  
        $kecamatan = auth()->user()->profile->kecamatan;                   
        $kelurahan = auth()->user()->profile->kelurahan; 
        $pos = $kecamatan.' '.$kelurahan;

        if(!auth()->user()->hasRole('admin')){            
            $reports->where('asal_kiriman','like','%'.auth()->user()->profile->area.'%');
            if(auth()->user()->hasRole('HUB akses')){
                $reports->where('mitra_myroket','like','%'.$kecamatan.'%');
            }else{
                $reports->where('mitra_myroket','like','%'.$pos.'%');
            }
        }
        $reports = $reports->get();        
        
        return view('pages.report.roket-kargo',[
            'reports'=>$reports,            
        ])
        ->layout('layout.app-layout',['title'=>'Myroket App | '.$this->title]);
    }

    /**
     * Update data
     */
    public function update()
    {        

        $row = DB::table('roket_kargo')->whereId($this->report_id);    


        $data = [
            'shipment_no' => $this->no_order,
            'nama_pengirim' => $this->nama_pengirim,
            'asal_kiriman' => $this->asal_kiriman,
            'tujuan_kiriman' => $this->tujuan_kiriman,
            'mitra_myroket' => $this->mitra_myroket,
        ];

        $row->update($data);

        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Diupdate']);
    }

   /**
     * Delete data
     */
    public function delete()
    {
        $report = DB::table('roket_kargo')->whereId($this->report_id);            
        $report->delete();       
        $this->_reset();
        return $this->emit('showAlert', ['msg' => 'Data Berhasil Dihapus']);
        
    }   

    public function getDataById($id)
    {        
        $report = DB::table('roket_kargo')->find($id);  
        
        $this->report_id = $id;
        $this->no_order = $report->shipment_no;
        $this->nama_pengirim = $report->nama_pengirim;
        $this->asal_kiriman = $report->asal_kiriman;
        $this->tujuan_kiriman = $report->tujuan_kiriman;
        $this->mitra_myroket = $report->mitra_myroket;
        

        $this->update = true;
        $this->emit('showModal');
    }

     public function getId($id)
    {
        $report = DB::table('roket_kargo')->find($id);        
        $this->report_id = $report->id;
        $this->emit('showModalConfirm');
    }

    public function showModal()
    {
        $this->emit('showModal');
    }

    
    public function _reset()
    {                
        $this->emit('closeModal');        
        $this->report_id = null;
        $this->no_order = null;
        $this->nama_pengirim = null;
        $this->asal_kiriman = null;
        $this->tujuan_kiriman = null;
        $this->mitra_myroket = null;

        $this->update = false;        
    }



}
