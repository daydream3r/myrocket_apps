<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\PartnerController;
use App\Http\Controllers\ListPartnerController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\PasswordController;
use App\Http\Controllers\TransactionController;

use App\Http\Controllers\Report\RoketCepatController;
use App\Http\Controllers\Report\RoketKargoController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('auth/login');
});

Route::prefix('auth')->group(function(){
    Route::get('login',[LoginController::class,'index'])->name('auth.login');
    Route::get('forgot-password',[LoginController::class,'forgot'])->name('auth.forgot');
    Route::get('new-password',[LoginController::class,'new'])->name('auth.new');
    Route::get('register',RegisterController::class)->name('auth.register');

    Route::post('login',[LoginController::class,'store'])->name('auth.login');
    Route::post('forgot-password',[LoginController::class,'forgot_save'])->name('auth.forgot-save');
    Route::post('reset-password',[LoginController::class,'new_save'])->name('auth.reset');

});

Route::middleware(['auth'])->group(function(){
    Route::get('dashboard',[DashboardController::class,'index'])->name('dashboard.index');
    Route::get('logout',[LoginController::class,'logout'])->name('logout');
    Route::get('permission',PermissionController::class)->name('permission');
    Route::get('role',RoleController::class)->name('role');
    Route::get('menu',MenuController::class)->name('menu');
    Route::get('user',UserController::class)->name('user');
    Route::get('partner',PartnerController::class)->name('partner');
    Route::get('list-partner',ListPartnerController::class)->name('list-partner');
    Route::get('profile',ProfileController::class)->name('profile');
    Route::get('change-password',[PasswordController::class,'index'])->name('change.password');
    Route::get('transaction/roket-cepat',[TransactionController::class,'cepat'])->name('transaction.cepat');
    Route::get('transaction/roket-pindahan',[TransactionController::class,'pindahan'])->name('transaction.pindahan');
    Route::get('transaction/roket-esok',[TransactionController::class,'esok'])->name('transaction.esok');
    Route::get('transaction/roket-regular',[TransactionController::class,'regular'])->name('transaction.regular');
    Route::get('transaction/roket-kilat',[TransactionController::class,'kilat'])->name('transaction.kilat');
    Route::get('transaction/roket-darat',[TransactionController::class,'darat'])->name('transaction.darat');
    Route::get('transaction/roket-plus',[TransactionController::class,'plus'])->name('transaction.plus');

    Route::post('change-password',[PasswordController::class,'store'])->name('change.password');

    Route::get('report/roket-cepat',RoketCepatController::class)->name('report.cepat');
    Route::get('report/roket-kargo',RoketKargoController::class)->name('report.kargo');


});



