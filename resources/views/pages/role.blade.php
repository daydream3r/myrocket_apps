<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Role</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">Role</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <div class="card-tools">   
            @can('create role')         
            <button type="button" wire:click="showModal" class="btn btn-primary btn-circle" title="Add New">
              Add New Role
            </button>            
            @endcan
          </div>
        </div>
        <div wire:ignore class="card-body">
          <table id="myTable" class="table table-striped projects">
              <thead>
                  <tr>
                      <th>
                          #
                      </th>
                      <th>
                          Role Name
                      </th>
                      <th>
                          Assign Role
                      </th>                                        
                      <th style="width: 20%">
                        #
                      </th>
                  </tr>
              </thead>
              <tbody>
                  @foreach($roles as $index => $role)
                    <tr>
                      <td>{{$index+1}}</td>                      
                      <td>{{$role->name}}</td>
                      <td><button class="btn btn-secondary" wire:click="getRoleId({{$role->id}})">Assign Role</button></td>
                      
                      <td>
                        @if($role->id <> 1)
                        <div class="d-flex">
                          @can('update role')
                          <button class="btn btn-success btn-xs mr-2" wire:click="getDataById({{$role->id}})" id="btn-edit-{{$role->id}}" style="margin-right: 10px;">edit</button>
                          @endcan
                          @can('delete role')
                        <button class="btn btn-danger btn-xs mr-2" wire:click="getId({{$role->id}})" id="btn-edit-{{$role->id}}" style="margin-right: 10px;">delete</button>
                        @endcan
                      </div>
                      @endif</td>
                        
                    </tr>
                  @endforeach
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  
  <!-- /.content-wrapper -->

  {{-- Modal Form --}}
  <div wire:ignore.self class="modal fade" id="form-modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        @unless($hasAssign)
        <div class="modal-header">                 
          <h5 class="modal-title">{{$update ? 'Update' : 'Add New'}} Permission</h5>
          <button type="button" class="close" wire:click="_reset" aria-label="Close">
                <i class="fas fa-times"></i>
              </button>
          </button>
        </div>
        <div class="modal-body">
          <div class="mb-3">
            <label class="form-label">Role Name</label>
            <input type="text" wire:model="name" placeholder="Name" class="form-control">
            @error('name')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger light" wire:click="_reset">Close</button>
          <button type="button" class="btn btn-success" wire:click="{{$update ? 'update' : 'store'}}">Save
            changes</button>
        </div>
        @else
        <div class="modal-header">                 
          <h5 class="modal-title">{{$update ? 'Update' : 'Add New'}} Permission</h5>
          <button type="button" class="close" wire:click="_reset" aria-label="Close">
                <i class="fas fa-times"></i>
              </button>
          </button>
        </div>
        <div class="modal-body">
          <div class="mb-3">
            <label class="form-label">Permission Table</label>
          </div>          
          <table class="table table-striped projects">
            <thead><tr>
            <th>Permision Name</th>
            <th>#</th>
            </tr>
            </thead>
            <tbody>
              @foreach($permissions as $permission)              
              <tr>
                <td>{{$permission->name}}</td>
                <td>                  
                   @if ($permission->roles->count() > 0)
                  <div class="form-check">
                    <input type="checkbox" wire:model="permission_ids" value="{{ $permission->id }}" name="permission_ids[]"class="form-check-input" {{ $permission->id == role_permission($role_id,$permission) ? 'checked' : '' }}>                                        
                  </div> 
                  @else
                  <div class="form-check">
                    <input type="checkbox" wire:model="permission_ids" value="{{ $permission->id }}" name="permission_ids[]"class="form-check-input">                                        
                  </div> 
                  @endif                 
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger light" wire:click="_reset">Close</button>
          <button type="button" class="btn btn-success" wire:click="assign">Save
            changes</button>
        </div>
        @endunless
      </div>
    </div>
  </div>

@include('having.modal')
</div>



  </div>


  @push('custom-scripts')
<script>
  document.addEventListener('livewire:load', function(e) {
            window.livewire.on('showModal', (data) => {
                $('#form-modal').modal('show')
            });

            window.livewire.on('showModalConfirm', (data) => {
                $('#confirm-modal').modal('show')
            });

            window.livewire.on('closeModal', (data) => {
                $('#confirm-modal').modal('hide')
                $('#form-modal').modal('hide')
            });

        })
</script>
@endpush
