<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Menu</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">Menu</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">          
          <div class="card-tools">     
          @can('create menu')       
            <button type="button" wire:click="showModal" class="btn btn-primary btn-circle" title="Add New">
              Add New Menu
            </button>  
            @endcan          
          </div>
        </div>
        <div wire:ignore class="card-body">
          <table id="myTable" class="table table-striped projects">
              <thead>
                  <tr>
                      <th>
                          #
                      </th>
                      <th>
                          Menu Name
                      </th>
                      <th>
                          Slug
                      </th> 
                      <th>
                          Parent
                      </th>  
                      <th>
                          Permission
                      </th> 
                      <th>
                          Position
                      </th>                      
                      <th style="width: 20%">
                        #
                      </th>
                  </tr>
              </thead>
              <tbody>
                  @foreach($menus as $index => $menu)
                    <tr>
                      <td>{{$index+1}}</td>
                      <td>{{$menu->name}}</td>
                      <td>{{$menu->slug}}</td>
                      <td>{{$menu->parent->name??'-'}}</td>
                      <td>{{Str::title($menu->permission->name)}}</td>
                      <td><strong>{{$menu->position}}</strong></td>
                      <td><div class="d-flex">
                        @can('update menu')
                        <button class="btn btn-success btn-xs mr-2" wire:click="getDataById({{$menu->id}})" id="btn-edit-{{$menu->id}}" style="margin-right: 10px;">edit</button>
                        @endcan
                        @can('delete menu')
                        <button class="btn btn-danger btn-xs mr-2" wire:click="getId({{$menu->id}})" id="btn-edit-{{$menu->id}}" style="margin-right: 10px;">delete</button>
                        @endcan
                      </div></td>
                    </tr>
                  @endforeach
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  
  <!-- /.content-wrapper -->

  {{-- Modal Form --}}
  <div wire:ignore.self class="modal fade" id="form-modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{$update ? 'Update' : 'Add New'}} Menu</h5>
          <button type="button" class="close" wire:click="_reset" aria-label="Close">
                <i class="fas fa-times"></i>
              </button>
          </button>
        </div>
        <div class="modal-body">
          <div class="mb-3">
            <label class="form-label">Menu Name</label>
            <input type="text" wire:model="name" placeholder="Settings" class="form-control">
            @error('name')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>
          <div class="mb-3">
            <label class="form-label">Parent</label>
            <select wire:model="parent_id" placeholder="Settings" class="form-control">
              <option value="">Pilih Parent</option>
              @foreach($parents as $parent)
                <option wire:key="{{$parent->id}}" value="{{$parent->id}}">{{Str::title($parent->name)}}</option>
              @endforeach
            </select>
            @error('parent_id')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>
            <div class="mb-3">
            <label class="form-label">Slug</label>
            <input type="text" wire:model="slug" placeholder="settings" class="form-control">
            @error('slug')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>
          <div class="mb-3">
            <label class="form-label">Position</label>
            <input type="number" wire:model="position" placeholder="1" class="form-control">
            @error('position')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>
          <div class="mb-3">
            <label class="form-label">Permission</label>
            <select wire:model="permission_id" placeholder="Settings" class="form-control">
              <option value="">Pilih Permission</option>
              @foreach($permissions as $permission)
                <option wire:key="{{$permission->id}}" value="{{$permission->id}}">{{Str::title($permission->name)}}</option>
              @endforeach
            </select>
            @error('permission_id')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger light" wire:click="_reset">Close</button>
          <button type="button" class="btn btn-success" wire:click="{{$update ? 'update' : 'store'}}">Save
            changes</button>
        </div>
      </div>
    </div>
  </div>
  @include('having.modal')
  </div>


  @push('custom-scripts')
<script>
  document.addEventListener('livewire:load', function(e) {
            window.livewire.on('showModal', (data) => {
                $('#form-modal').modal('show')
            });

            window.livewire.on('showModalConfirm', (data) => {
                $('#confirm-modal').modal('show')
            });

            window.livewire.on('closeModal', (data) => {
                $('#confirm-modal').modal('hide')
                $('#form-modal').modal('hide')
            });            

        })
</script>
@endpush
