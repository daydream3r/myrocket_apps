<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Roket Cepat</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">Roket Cepat</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <div class="card-tools">                              
          </div>
        </div>
        <div wire:ignore class="card-body">
          <table id="myTable" class="table table-striped projects">
              <thead>
                  <tr>
                      <th>
                          #
                      </th>
                      <th>
                          No Order
                      </th>
                      <th>
                          Pengguna
                      </th>  
                      <th>
                          Pembayaran
                      </th>    
                      <th>
                          Total Produk
                      </th>    
                      <th>
                          Total Jasa
                      </th> 
                      <th>
                          Total Kurir
                      </th>          
                      <th>
                          Total Tambahan
                      </th>
                      <th>
                          Sub Total
                      </th>
                      <th>
                          Total
                      </th>
                      <th>
                          Nama Mitra
                      </th>
                      <th>
                          Mitra
                      </th>
                      <th>
                          Driver
                      </th>
                      <th>
                          Nama Driver
                      </th>
                      <th>
                          Asal
                      </th><th>
                          Tujuan
                      </th>
                      <th>
                          Diskon
                      </th>
                      <th>
                          Kode Voucher
                      </th>
                      <th>
                          Status
                      </th>

                      <th style="width: 20%">
                        #
                      </th>
                  </tr>
              </thead>
              <tbody>
                  @foreach($reports as $index => $report)
                    <tr>
                      <td>{{$index+1}}</td>
                      <td>{{$report->no_order}}</td>                      
                      <td>{{$report->nama_pengguna}}</td>                      
                      <td>{{$report->pembayaran}}</td>                      
                      <td>{{formatRupiah($report->total_produk)}}</td>                      
                      <td>{{formatRupiah($report->total_jasa)}}</td>                      
                      <td>{{formatRupiah($report->total_kurir)}}</td>                      
                      <td>{{formatRupiah($report->total_tambahan)}}</td>                      
                      <td>{{formatRupiah($report->sub_total)}}</td>                      
                      <td>{{formatRupiah($report->total)}}</td>                      
                      <td>{{$report->nama_mitra}}</td>                      
                      <td>{{$report->mitra}}</td>                      
                      <td>{{$report->driver}}</td>                      
                      <td>{{$report->nama_driver}}</td>                      
                      <td>{{$report->asal}}</td>                      
                      <td>{{$report->tujuan}}</td>                      
                      <td>{{$report->diskon}}</td>                      
                      <td>{{$report->kode_voucher}}</td>                      
                      <td>{{$report->status}}</td>       
                      <td><div class="d-flex">
                        @can('update report')
                        <button class="btn btn-success btn-xs mr-2" wire:click="getDataById({{$report->id}})" id="btn-edit-{{$report->id}}" style="margin-right: 10px;">edit</button>
                        @endcan
                        @can('delete report')
                        <button class="btn btn-danger btn-xs mr-2" wire:click.prevent="getId({{$report->id}})" id="btn-edit-{{$report->id}}" style="margin-right: 10px;">delete</button>
                        @endcan
                      </div></td>
                    </tr>
                  @endforeach
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  
  <!-- /.content-wrapper -->

  {{-- Modal Form --}}
  <div wire:ignore.self class="modal fade" id="form-modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{$update ? 'Update' : 'Add New'}} Roket Cepat</h5>
          <button type="button" class="close" wire:click="_reset" aria-label="Close">
                <i class="fas fa-times"></i>
              </button>
          </button>
        </div>
        <div class="modal-body">
          <div class="mb-3">
            <label class="form-label">No Order</label>
            <input type="text" wire:model="no_order" placeholder="No Order" class="form-control">
            @error('no_order')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>  
          <div class="mb-3">
            <label class="form-label">Pembayaran</label>
            <input type="text" wire:model="pembayaran" placeholder="Pembayaran" class="form-control">
            @error('pembayaran')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>            
          <div class="mb-3">
            <label class="form-label">Total Produk</label>
            <input type="number" wire:model="total_produk" placeholder="Total Produk" class="form-control">
            @error('total_produk')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>      
          <div class="mb-3">
            <label class="form-label">Total Jasa</label>
            <input type="number" wire:model="total_jasa" placeholder="Total Jasa" class="form-control">
            @error('total_jasa')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>      
          <div class="mb-3">
            <label class="form-label">Total Kurir</label>
            <input type="number" wire:model="total_kurir" placeholder="Total Kurir" class="form-control">
            @error('total_kurir')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>   
          <div class="mb-3">
            <label class="form-label">Total Tambahan</label>
            <input type="number" wire:model="total_tambahan" placeholder="Total Tambahan" class="form-control">
            @error('total_tambahan')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div> 
          <div class="mb-3">
            <label class="form-label">Sub Total</label>
            <input type="number" wire:model="sub_total" placeholder="Sub Total" class="form-control">
            @error('sub_total')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>  
          <div class="mb-3">
            <label class="form-label">Total</label>
            <input type="number" wire:model="total" placeholder="Total" class="form-control">
            @error('total')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div> 
          <div class="mb-3">
            <label class="form-label">Nama Mitra</label>
            <input type="text" wire:model="nama_mitra" placeholder="Nama Mitra" class="form-control">
            @error('nama_mitra')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>
          <div class="mb-3">
            <label class="form-label">Mitra</label>
            <input type="text" wire:model="mitra" placeholder="Mitra" class="form-control">
            @error('mitra')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>
          <div class="mb-3">
            <label class="form-label">Driver</label>
            <input type="text" wire:model="driver" placeholder="Driver" class="form-control">
            @error('driver')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>            
          <div class="mb-3">
            <label class="form-label">Nama Driver</label>
            <input type="text" wire:model="nama_driver" placeholder="Nama Driver" class="form-control">
            @error('nama_driver')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>  
          <div class="mb-3">
            <label class="form-label">Asal</label>
            <input type="text" wire:model="asal" placeholder="Asal" class="form-control">
            @error('asal')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div> 
          <div class="mb-3">
            <label class="form-label">Tujuan</label>
            <input type="text" wire:model="tujuan" placeholder="Tujuan" class="form-control">
            @error('tujuan')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>
          <div class="mb-3">
            <label class="form-label">Diskon</label>
            <input type="number" wire:model="diskon" placeholder="Diskon" class="form-control">
            @error('diskon')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>  
          <div class="mb-3">
            <label class="form-label">Kode Voucher</label>
            <input type="text" wire:model="kode_voucher" placeholder="Kode Voucher" class="form-control">
            @error('kode_voucher')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>
          <div class="mb-3">
            <label class="form-label">Status</label>
            <input type="text" wire:model="status" placeholder="Status" class="form-control">
            @error('status')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger light" wire:click="_reset">Close</button>
          <button type="button" class="btn btn-success" wire:click="{{$update ? 'update' : 'store'}}">Save
            changes</button>
        </div>
      </div>
    </div>
    </div>

@include('having.modal')
</div>



  </div>


  @push('custom-scripts')
<script>
  document.addEventListener('livewire:load', function(e) {
            window.livewire.on('showModal', (data) => {
                $('#form-modal').modal('show')
            });

            window.livewire.on('showModalConfirm', (data) => {
                $('#confirm-modal').modal('show')
            });

            window.livewire.on('closeModal', (data) => {
                $('#confirm-modal').modal('hide')
                $('#form-modal').modal('hide')
            });

        })
</script>
@endpush
