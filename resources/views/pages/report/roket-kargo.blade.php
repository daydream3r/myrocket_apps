<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Roket Kargo</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">Roket Kargo</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <div class="card-tools">                              
          </div>
        </div>
        <div wire:ignore class="card-body">
          <table id="myTable" class="table table-striped projects">
              <thead>
                  <tr>
                      <th>
                          #
                      </th>
                      <th>
                          No Shipment
                      </th>
                      <th>
                          Nama Pengirim
                      </th>   
                      <th>
                        Pengiriman
                      </th>                                              
                      <th>
                          Alamat Pengirim
                      </th>
                      <th>
                          Posisi Pengirim
                      </th>                      
                      <th>
                          Nama Penerima
                      </th>                                                                
                      <th>
                          Alamat Penerima
                      </th>
                      <th>
                          Posisi Penerima
                      </th>                      
                      <th>
                          Barang
                      </th>                                            
                      <th>
                          Waktu Jemput
                      </th>                      
                      <th>
                          Biaya Kirim
                      </th>
                      <th>
                          Jenis Pembayaran
                      </th>                                                                  
                      <th>
                          Mitra MyRoket
                      </th>                      

                      <th style="width: 20%">
                        #
                      </th>
                  </tr>
              </thead>
              <tbody>
                  @foreach($reports as $index => $report)
                    <tr>
                      <td>{{$index+1}}</td>
                      <td>{{$report->shipment_no}}</td>
                      <td>{{$report->nama_pengirim}}</td>
                      <td>{{$report->asal_kiriman}} - {{$report->tujuan_kiriman}}</td>
                      <td>{{$report->kecamatan_pengirim}} {{$report->kelurahan_pengirim}} {{$report->alamat_pengirim}} {{$report->kodepos_pengirim}}</td>
                      <td>{{$report->posisi_pengirim}}</td>
                      <td>{{$report->nama_penerima}}</td>
                      <td>{{$report->kecamatan_penerima}} {{$report->kelurahan_penerima}} {{$report->alamat_penerima}} {{$report->kodepos_penerima}} - {{$report->hp_penerima}}</td>
                      <td>{{$report->posisi_penerima}}</td>
                      <td><ul>
                        <li>Type:{{$report->type_barang}}</li>
                        <li>Detail:{{$report->detail_barang}}</li>
                        <li>Berat:{{$report->berat_timbang}}kg</li>
                        <li>dimensi:{{$report->P}}x{{$report->L}}x{{$report->T}}</li>
                        <li>Harga:{{$report->harga_barang}}</li>
                      </ul></td>
                      <td>{{$report->tanggal_jemput}} {{$report->waktu_jemput}}</td>
                      <td>{{formatRupiah($report->biaya_kirim_calc)}}</td>
                      <td>{{$report->jenis_pembayaran}}</td>                      
                      <td>{{$report->mitra_myroket}}</td>     
                      <td><div class="d-flex">
                        @can('update report')
                        <button class="btn btn-success btn-xs mr-2" wire:click.prevent="getDataById({{$report->id}})" id="btn-edit-{{$report->id}}" style="margin-right: 10px;">edit</button>
                        @endcan
                        @can('delete report')
                        <button class="btn btn-danger btn-xs mr-2" wire:click.prevent="getId({{$report->id}})" id="btn-edit-{{$report->id}}" style="margin-right: 10px;">delete</button>
                        @endcan
                      </div></td>
                    </tr>
                  @endforeach
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  
  <!-- /.content-wrapper -->

  {{-- Modal Form --}}
  <div wire:ignore.self class="modal fade" id="form-modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{$update ? 'Update' : 'Add New'}} Roket Kargo</h5>
          <button type="button" class="close" wire:click="_reset" aria-label="Close">
                <i class="fas fa-times"></i>
              </button>
          </button>
        </div>
        <div class="modal-body">
          <div class="mb-3">
            <label class="form-label">No Order</label>
            <input type="text" wire:model="no_order" placeholder="No Order" class="form-control">
            @error('no_order')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>  
          <div class="mb-3">
            <label class="form-label">Nama Pengirim</label>
            <input type="text" wire:model="nama_pengirim" placeholder="Nama Pengirim" class="form-control">
            @error('nama_pengirim')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>            
          <div class="mb-3">
            <label class="form-label">Asal Pengirim</label>
            <input type="text" wire:model="asal_kiriman" placeholder="Asal Pengirim" class="form-control">
            @error('asal_kiriman')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div> 
          <div class="mb-3">
            <label class="form-label">Tujuan Pengirim</label>
            <input type="text" wire:model="tujuan_kiriman" placeholder="Tujuan Pengirim" class="form-control">
            @error('tujuan_kiriman')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>      
          <div class="mb-3">
            <label class="form-label">Mitra MyRoket</label>
            <input type="text" wire:model="mitra_myroket" placeholder="Mitra Pengirim" class="form-control">
            @error('mitra_myroket')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>      
                   
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger light" wire:click="_reset">Close</button>
          <button type="button" class="btn btn-success" wire:click="{{$update ? 'update' : 'store'}}">Save
            changes</button>
        </div>
      </div>
      </div>
    </div>

@include('having.modal')
</div>



  </div>


  @push('custom-scripts')
<script>
  document.addEventListener('livewire:load', function(e) {
            window.livewire.on('showModal', (data) => {
                $('#form-modal').modal('show')
            });

            window.livewire.on('showModalConfirm', (data) => {
                $('#confirm-modal').modal('show')
            });

            window.livewire.on('closeModal', (data) => {
                $('#confirm-modal').modal('hide')
                $('#form-modal').modal('hide')
            });

        })
</script>
@endpush
