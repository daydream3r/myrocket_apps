<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>List Partner</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">List Partner</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <div class="card-tools">   
            @can('create partner')          
            <button type="button" wire:click="showModal" class="btn btn-primary btn-circle" title="Add New">
              Add New Partner
            </button>      
            @endcan                              
          </div>
        </div>
        <div wire:ignore class="card-body">
          <table id="myTable" class="table table-striped projects">
              <thead>
                  <tr>
                      <th>
                          #
                      </th>
                      <th>
                          Partner Name
                      </th>
                      <th>
                          Daerah
                      </th>                      
                      <th>
                          PIC
                      </th>   
                      <th>
                          Report To
                      </th>  
                      <th>
                          Partner
                      </th> 
                      
                      <th>
                          Email
                      </th>    
                      <th>
                          HP
                      </th>                                        
                      <th style="width: 20%">
                        #
                      </th>
                  </tr>
              </thead>
              <tbody>
                  @foreach($partners as $index => $partner)
                    <tr>
                      <td>{{$index+1}}</td>
                      <td>{{$partner->partner_name}} {{$partner->area}}</td>     
                      <td>{{$partner->kecamatan}}-{{$partner->kelurahan}}</td>              
                      <td>{{$partner->name}}</td>
                      <td>{{getPICName($partner->report_to)}}</td>
                      <td>{{$partner->partner_name}}</td>
                      <td>{{$partner->email}}</td>
                      <td>{{$partner->phone_number}}</td>
                      <td><div class="d-flex">
                        @can('update partner')
                        <button class="btn btn-success btn-xs mr-2" wire:click="getDataById({{$partner->id}})" id="btn-edit-{{$partner->id}}" style="margin-right: 10px;">edit</button>
                        @endcan
                        @can('delete partner')
                        <button class="btn btn-danger btn-xs mr-2" wire:click="getId({{$partner->id}})" id="btn-edit-{{$partner->id}}" style="margin-right: 10px;">delete</button>
                        @endcan
                      </div></td>
                    </tr>
                  @endforeach
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  
  <!-- /.content-wrapper -->

  {{-- Modal Form --}}
  <div wire:ignore.self class="modal fade" id="form-modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{$update ? 'Update' : 'Add New'}} Partner</h5>
          <button type="button" class="close" wire:click="_reset" aria-label="Close">
                <i class="fas fa-times"></i>
              </button>
          </button>
        </div>
        <div class="modal-body">
           
          <div class="mb-3">
            <label class="form-label">Area</label>
            <input type="text" wire:model="area" placeholder="Area" class="form-control">
            @error('area')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div> 
          <div class="mb-3">
            <label class="form-label">Kecamatan</label>
            <input type="text" wire:model="kecamatan" placeholder="Kecamatan" class="form-control">
            @error('kecamatan')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div> <div class="mb-3">
            <label class="form-label">Kelurahan</label>
            <input type="text" wire:model="kelurahan" placeholder="Kelurahan" class="form-control">
            @error('kelurahan')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>  

          <div class="mb-3">
            <label class="form-label">Partner</label>
            <select wire:model="partner_id" class="form-control">
              <option value="">Pilih Partner</option>
              @foreach($list_partner as $key => $list)
              <option value="{{$list->id}}">{{$list->name}}</option>
              @endforeach
            </select>
            @error('partner_id')
            <small class="text-danger">{{ $message }}</small>
            @enderror  
            </div>       
            @if(!$update) 
          <div class="mb-3">
            <label class="form-label">PIC</label>
            <select wire:model="pic" class="form-control">
              <option value="">Pilih PIC</option>
              @foreach($users as $key => $pic)
              <option value="{{$pic->id}}">{{$pic->name}}</option>
              @endforeach
            </select>
            @error('pic')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>       
          @endif    
          <div class="mb-3">
            <label class="form-label">Report To</label>
            <select wire:model="assignTo" placeholder="Settings" class="form-control">
              <option value="">Pilih User</option>
              @foreach($principles as $user)
                <option wire:key="{{$user->id}}" value="{{$user->id}}">{{Str::title($user->name)}}</option>
              @endforeach
            </select>
            @error('assignTo')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>         
          
        </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger light" wire:click="_reset">Close</button>
            <button type="button" class="btn btn-success" wire:click="{{$update ? 'update' : 'store'}}">Save
              changes</button>
          </div>
      </div>
    </div>
  </div>
  

@include('having.modal')
</div>



  </div>


  @push('custom-scripts')
<script>
  document.addEventListener('livewire:load', function(e) {
            window.livewire.on('showModal', (data) => {
                $('#form-modal').modal('show')
            });

            window.livewire.on('showModalConfirm', (data) => {
                $('#confirm-modal').modal('show')
            });

            window.livewire.on('closeModal', (data) => {
                $('#confirm-modal').modal('hide')
                $('#form-modal').modal('hide')
            });

        })
</script>
@endpush
