@extends('layout.app-layout')
@section('title')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Change Password</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">Change Password</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      @if(session('success'))
          <div class="text-success text-sm my-2"><span>{{session('success')}}</span></div>
        @endif
      <form action="{{route('change.password')}}" method="POST">
        @csrf
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">            
            <div class="card-body">
              <div class="form-group">
                <label for="inputName">Password</label>
                <input type="password" class="form-control" name="password">
                @error('password')
            <small class="text-danger">{{ $message }}</small>
            @enderror
              </div>
              <div class="form-group">
                <label for="inputName">Password Confirmation</label>
                <input type="password" class="form-control" name="password_confirmation">
                @error('password_confirmation')
            <small class="text-danger">{{ $message }}</small>
            @enderror
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      
      </div>
      <div class="row">
        <div class="col-12">
          <a href="{{route('dashboard.index')}}" class="btn btn-secondary">Cancel</a>
          <input type="submit" value="Submit" class="btn btn-primary float-right">
        </div>
      </div>
      </form>
    </section>
    <!-- /.content -->
  
  <!-- /.content-wrapper -->

</div>
@endsection
