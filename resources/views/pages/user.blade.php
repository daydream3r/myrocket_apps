<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>User</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">User</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <div class="card-tools"> 
            @can('create user')           
            <button type="button" wire:click="showModal" wire:ignore class="btn btn-primary btn-circle" title="Add New">
              Add New User
            </button>      
            @endcan      
          </div>
        </div>
        <div wire:ignore class="card-body">
          <table id="myTable" class="table table-striped projects">
              <thead>
                  <tr>
                      <th>
                          #
                      </th>
                      <th>
                          Name
                      </th>
                      <th>
                          Email
                      </th>
                      @can('verify user') 
                      <th>
                          Verified By
                      </th>  
                      @endcan
                      <th>
                          Phone Number
                      </th> 
                      <th>
                          Last Login
                      </th>
                      <th>
                        Role
                      </th> 
                      <th>
                          Status
                      </th>                                          
                      <th style="width: 20%">
                        #
                      </th>
                  </tr>
              </thead>
              <tbody>
                  @foreach($users as $index => $user)
                    <tr>
                      <td>{{$index+1}}</td>
                      <td>{{$user->name}}</td>
                      <td>{{$user->email}}</td>
                      <td>
                          @if(!empty($user->verified->name))
                          {{$user->verified->name}}
                          @else
                          <button class="btn btn-sm btn-primary" wire:click="verify({{$user->id}})">verify user</button>
                        @endif
                      </td>
                      <td>{{$user->profile->phone_number??'-'}}</td>                      
                      <td>{{$user->last_login?Carbon\Carbon::parse($user->last_login)->format('Y-m-d'):'-'}}</td>
                      @can('verify user')
                      <td>{{$user->getRole[0]->name??'-'}}</td>
                      @endcan
                      <td>{{status($user->status)}}</td>
                      <td><div class="d-flex">
                        @can('update user')   
                        <button class="btn btn-success btn-xs mr-2" wire:click="getDataById({{$user->id}})" id="btn-edit-{{$user->id}}" style="margin-right: 10px;">edit</button>
                        @endcan
                        @can('delete user')
                        <button class="btn btn-danger btn-xs mr-2" wire:click="getId({{$user->id}})" id="btn-edit-{{$user->id}}" style="margin-right: 10px;">delete</button></div></td>
                        @endcan
                    </tr>
                  @endforeach
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  
  <!-- /.content-wrapper -->

  {{-- Modal Form --}}
  <div wire:ignore.self class="modal fade" id="form-modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">{{$update ? 'Update' : 'Add New'}} User</h5>
          <button type="button" class="close" wire:click="_reset" aria-label="Close">
                <i class="fas fa-times"></i>
              </button>
          </button>
        </div>
        <div class="modal-body">
          @error('valid')
          <small class="text-danger">{{ $message }}</small>
          @enderror
          <div class="mb-3">
            <label class="form-label">Name</label>
            <input type="text" wire:model="name" placeholder="myroket" class="form-control">
            @error('name')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>
          
            <div class="mb-3">
            <label class="form-label">Email</label>
            <input type="text" wire:model="email" placeholder="info@myroket.com" class="form-control" {{$update?'readonly':''}}>
            @error('email')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>
          <div class="mb-3">
            <label class="form-label">Phone Number</label>
            <input type="text" wire:model="phone_number" placeholder="08182882890" class="form-control">
            @error('phone_number')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>                
          <div class="mb-3">
            <label class="form-label">Role</label>
            <select wire:model="role_id" placeholder="Settings" class="form-control">
              <option value="">Pilih Role</option>
              @foreach($roles as $role)
                <option wire:key="{{$role->id}}" value="{{$role->id}}">{{Str::title($role->name)}}</option>
              @endforeach
            </select>
            @error('role_id')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>
          @if($update)
          <div class="mb-3">
            <label class="form-label">Status</label>
            <select wire:model="status" class="form-control">
              <option value="">Pilih Status</option>
              <option value="1">Aktif</option>
              <option value="0">Nonaktif</option>
              
            </select>
            @error('status')
            <small class="text-danger">{{ $message }}</small>
            @enderror
          </div>
          @endif
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger light" wire:click="_reset">Close</button>
          <button type="button" class="btn btn-success" wire:click="{{$update ? 'update' : 'store'}}">Save
            changes</button>
        </div>
      </div>
    </div>
  </div>
  @include('having.modal')
  {{-- Modal confirm --}}
  <div id="verify-modal" wire:ignore.self class="modal fade" tabindex="-1" permission="dialog"
    aria-labelledby="my-modal-title" aria-hidden="true">
    <div class="modal-dialog" permission="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="my-modal-title">Verify User!</h5>
        </div>
        <div class="modal-body">
          <p>Are you sure to verify this user?</p>
        </div>
        <div class="modal-footer">
          <button type="submit" wire:click='verifyNow' class="btn btn-danger btn-sm"><i class="fa fa-check pr-2"></i> Yes,
            Verify It</button>
          <button class="btn btn-secondary btn-sm" wire:click='_reset'><i class="fa fa-times pr-2"></i>
            Cancel</a>
        </div>
      </div>
    </div>
  </div>
  </div>


  @push('custom-scripts')
<script>        
  document.addEventListener('livewire:load', function(e) {
            window.livewire.on('showModal', (data) => {
                $('#form-modal').modal('show')
            });

            window.livewire.on('showModalConfirm', (data) => {
                $('#confirm-modal').modal('show')
            });

            window.livewire.on('showModalVerify', (data) => {
                $('#verify-modal').modal('show')
            });

            window.livewire.on('closeModal', (data) => {
                $('#confirm-modal').modal('hide')
                $('#form-modal').modal('hide')
                $('#verify-modal').modal('hide')
            });
        })
</script>
@endpush
