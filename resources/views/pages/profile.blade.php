
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profile</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">Profile</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      @if(session('success'))
          <div class="text-success text-sm my-2"><span>{{session('success')}}</span></div>
        @endif
      <form action="{{route('profile')}}" wire:submit.prevent="store" method="POST">
        @csrf
        <div class="card card-primary">
          <div class="card-body">
      <div class="row">
        <div class="col-md-12">                                                
                <div class="form-group">                  
                    @if($profile_picture)
                      <img width="50" src="{{url('storage/'.$profile_picture)}}" alt="{{$name}}"><br>
                    @endif
                        <label class="form-control-label" >Change Profile</label>
                        <input wire:model="profile" type="file" name="profile" class="form-control-file" >
                      </div>                    
          @error('profile')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror    
              </div>                        
          <!-- /.card -->        
        <div class="col-md-6">                  
              <div class="form-group">
                <label for="inputName">Name</label>
                <input type="text" class="form-control" wire:model="name" value="{{$name}}">
                @error('name')
            <small class="text-danger">{{ $message }}</small>
            @enderror
              </div>                        
          <!-- /.card -->
        </div>
        <div class="col-md-6">
          <div class="form-group">
                <label for="inputName">Email</label>
                <input type="email" class="form-control" wire:model="email" value="{{$email}}" readonly>
                @error('email')
            <small class="text-danger">{{ $message }}</small>
            @enderror
              </div> 
        </div>
        <div class="col-md-6">
          <div class="form-group">
                <label for="inputName">Phone Number</label>
                <input type="text" class="form-control" wire:model="phone_number" value="{{$phone_number}}">
                @error('phone_number')
            <small class="text-danger">{{ $message }}</small>
            @enderror
              </div> 
        </div>
        <div class="col-md-6">
          
        </div>
        {{-- <div class="col-md-6">
          <div class="form-group">
                <label for="inputName">Mitra</label>
                <select name="" id="" wire:model="mitra_id" class="form-control"  placeholder="mitra">
            <option value="">-Pilih Mitra-</option>
            @foreach($partners as $partner)
              <option wire:key="{{$partner->id}}" value="{{$partner->id}}">{{Str::title($partner->name)}}</option>
            @endforeach
          </select>  
                @error('mitra_id')
            <small class="text-danger">{{ $message }}</small>
            @enderror
              </div> 
        </div>        
        <div class="col-md-6">
          <div class="form-group">
                <label for="inputName">Mitra Name</label>
                <input type="text" class="form-control" wire:model="mitra" value="{{$mitra}}">
                @error('mitra')
            <small class="text-danger">{{ $message }}</small>
            @enderror
              </div> 
        </div> --}}
        <div class="col-md-12">
          <div class="form-group">
                <label for="inputName">Address</label>
                <textarea name="address" wire:model="address" class="form-control" value="{{$address}}" placeholder="Address"></textarea>   
                @error('address')
            <small class="text-danger">{{ $message }}</small>
            @enderror
              </div> 
        </div>
        <div class="col-md-4">
          <div class="form-group">
                <label for="inputName">Kelurahan</label>
                <input type="text" class="form-control" wire:model="kelurahan" value="{{$kelurahan}}">
                @error('kelurahan')
            <small class="text-danger">{{ $message }}</small>
            @enderror
              </div> 
        </div>
        <div class="col-md-4">
          <div class="form-group">
                <label for="inputName">Kecamatan</label>
                <input type="text" class="form-control" wire:model="kecamatan" value="{{$kecamatan}}">
                @error('kecamatan')
            <small class="text-danger">{{ $message }}</small>
            @enderror
              </div> 
        </div>
        <div class="col-md-4">
          <div class="form-group">
                <label for="inputName">Provinsi</label>
                <input type="text" class="form-control" wire:model="provinsi" value="{{$provinsi}}">
                @error('provinsi')
            <small class="text-danger">{{ $message }}</small>
            @enderror
              </div> 
        </div>
        <div class="col-md-4">
          <div class="form-group">
                <label for="inputName">Kode Pos</label>
                <input type="text" class="form-control" wire:model="kode_pos" value="{{$kode_pos}}">
                @error('kode_pos')
            <small class="text-danger">{{ $message }}</small>
            @enderror
              </div> 
        </div>
        <div class="col-md-8">
          <div class="form-group">
                <label for="inputName">Google Maps</label>
                <input type="text" class="form-control" wire:model="gmap" value="{{$gmap}}">
                @error('gmap')
            <small class="text-danger">{{ $message }}</small>
            @enderror
              </div> 
        </div>

      
      </div>
      </div>
      </div>
      <div class="row">
        <div class="col-12">
          <a href="{{route('dashboard.index')}}" class="btn btn-secondary">Cancel</a>
          <input type="submit" value="Submit" class="btn btn-primary float-right">
        </div>
      </div>
      </form>
    </section>
    <!-- /.content -->
  
  <!-- /.content-wrapper -->

</div>

