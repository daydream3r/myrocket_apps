{{-- Modal confirm --}}
  <div id="confirm-modal" wire:ignore.self class="modal fade" tabindex="-1" permission="dialog"
    aria-labelledby="my-modal-title" aria-hidden="true">
    <div class="modal-dialog" permission="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="my-modal-title">Delete Confirmation!</h5>
        </div>
        <div class="modal-body">
          <p>Are you sure to delete this data?</p>
        </div>
        <div class="modal-footer">
          <button type="submit" wire:click='delete' class="btn btn-danger btn-sm"><i class="fa fa-check pr-2"></i> Yes,
            Delete It</button>
          <button class="btn btn-secondary btn-sm" wire:click='_reset'><i class="fa fa-times pr-2"></i>
            Cancel</a>
        </div>
      </div>
    </div>
  </div>