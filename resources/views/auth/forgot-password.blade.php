@extends('layout.auth-layout')
@section('title','Myrocket | Forgot Password')
@section('content')

  <div class="login-logo">
    <a href="{{route('auth.forgot-save')}}"><img class="mr-1" width="50" src="{{url('images/logo.png')}}" alt=""><strong>MyRoket</strong> by JOPAR</a>
  </div>
  <!-- /.login-logo -->
  <div class="card card-outline card-primary">    
    <div class="card-body">
      <p class="login-box-msg">Enter your valid Email</p>

      <form action="{{route('auth.forgot')}}" method="POST">
        @csrf
        @if(session()->has('error'))
          <div class="text-danger text-sm my-1"><span>{{session('error')}}</span></div>
        @endif
        <div class="mb-3">
          <input type="email" name="email" class="form-control" value="{{old('email')}}" placeholder="Email">   
          @error('email')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror       
        </div>        

        <div class="row">          
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Submit</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <p class="mt-2">
        <a href="{{route('auth.login')}}">Already Remember Password?</a>
      </p>      
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
@endsection