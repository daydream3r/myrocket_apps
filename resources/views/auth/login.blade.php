@extends('layout.auth-layout')
@section('title','Myrocket | Login')
@section('content')

  <div class="login-logo">
    <a href="{{route('auth.login')}}"><img class="mr-1" width="50" src="{{url('images/logo.png')}}" alt=""><strong>MyRoket</strong> by JOPAR</a>
  </div>
  <!-- /.login-logo -->
  <div class="card card-outline card-primary">    
    <div class="card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form action="{{route('auth.login')}}" method="POST">
        @csrf
        @if(session()->has('error'))
          <div class="text-danger text-sm my-1"><span>{{session('error')}}</span></div>
        @endif
        @if(session()->has('success'))
          <div class="text-success text-sm my-1"><span>{{session('success')}}</span></div>
        @endif
        <div class="mb-3">
          <input type="email" name="email" class="form-control" value="{{old('email')}}" placeholder="Email">   
          @error('email')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror       
        </div>
        <div class="mb-3">
          <input type="password" name="password" class="form-control" placeholder="Password">       
          @error('password')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror              
        </div>

        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <p class="mb-1">
        <a href="{{route('auth.forgot')}}">Lupa Password?</a>
      </p>
      <p class="mb-1">
        <a href="{{route('auth.register')}}">Register New Membership?</a>
      </p>
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
@endsection