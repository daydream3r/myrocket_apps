
  <div>
  <div class="login-logo">
    <a href="{{route('auth.register')}}"><img class="mr-1" width="50" src="{{url('images/logo.png')}}" alt=""><strong>MyRoket</strong> by JOPAR</a>
  </div>
  <!-- /.login-logo -->
  <div class="card card-outline card-primary">    
    <div class="card-body">
      <p class=""><strong>Register New Membership</strong></p>      
      <form method="POST" wire:submit.prevent="store">
        @csrf
        @error('valid')
          <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
        @enderror
        <div class="mb-3">
          <input type="text" wire:model="name" class="form-control" value="{{$name}}" placeholder="Full Name">   

          @error('name')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror       
        </div> 
        <div class="mb-3">
          <input type="email" name="email" wire:model="email" class="form-control" value="{{$email}}" placeholder="Email">   

          @error('email')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror       
        </div>
        <div class="mb-3">
          <input type="number" name="phone_number" wire:model="phone_number" class="form-control" value="{{$phone_number}}" placeholder="No HP">   

          @error('phone_number')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror       
        </div>
        <div class="mb-3">
          <input type="password" wire:model="password" class="form-control" value="{{$password}}" placeholder="Password">   

          @error('password')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror       
        </div>
        <div class="mb-3">
          <input type="password" wire:model="password_confirmation" class="form-control" value="{{$password_confirmation}}" placeholder="Confirmation Password">   

          @error('password_confirmation')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror       
        </div> 
{{-- 
        <div class="mb-3">
          <select name="" id="" wire:model="mitra_id" class="form-control"  placeholder="mitra">
            <option value="">-Pilih Mitra-</option>
            @foreach($partners as $partner)
              <option wire:key="{{$partner->id}}" value="{{$partner->id}}">{{Str::title($partner->name)}}</option>
            @endforeach
          </select>  

          @error('mitra_id')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror       
        </div> 
        <div class="mb-3">
          <input type="text" wire:model="mitra" name="mitra" class="form-control" value="{{$mitra}}" placeholder="Mitra Name">   

          @error('mitra')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror       
        </div> --}}
         
         <div class="mb-3">
          <textarea name="address" wire:model="address" class="form-control" value="{{$address}}" placeholder="Address"></textarea>   

          @error('address')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror       
        </div> 
        <div class="mb-3">
          <input type="text" wire:model="kelurahan" name="kelurahan" class="form-control" value="{{$kelurahan}}" placeholder="Kelurahan"/>
          @error('kelurahan')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror       
        </div>  
        <div class="mb-3">
          <input type="text" wire:model="kecamatan" name="kecamatan" class="form-control" value="{{$kecamatan}}" placeholder="Kecamatan"/>
          @error('kecamatan')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror       
        </div>   
        <div class="mb-3">
          <input type="text" wire:model="provinsi" name="provinsi" class="form-control" value="{{$provinsi}}" placeholder="Provinsi"/>
          @error('provinsi')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror       
        </div><div class="mb-3">
          <input type="text" wire:model="kode_pos" name="kode_pos" class="form-control" value="{{$kode_pos}}" placeholder="Kode Pos"/>
          @error('kode_pos')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror       
        </div>    
        <div class="mb-3">
            <input type="text" wire:model="gmap" name="gmap" class="form-control" value="{{$gmap}}" placeholder="Google Map Link"/>
          @error('gmap')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror       
        </div>  
        <div class="mb-3">            
            
                      <div class="form-group">
                        <label class="form-control-label" >Upload Profile</label>
                        <input wire:model="profile" type="file" name="profile" class="form-control-file" >
                      </div>                    
          @error('profile')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror       
        </div> <div class="mb-3">            
            
                      <div class="form-group">
                        <label class="form-control-label" >Upload KTP</label>
                        <input type="file" wire:model="ktp" name="ktp" class="form-control-file" >
                      </div>
                    
          @error('ktp')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror       
        </div>  
        <div class="mb-3">
            
                      <div class="form-group">
                        <label class="form-control-label">Upload KK</label>
                        <input type="file" wire:model="kk" name="kk" class="form-control-file">
                        
                      </div>
                    
          @error('kk')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror       
        </div> 
        <div class="mb-3">            
                      <div class="form-group">
                        <label class="form-control-label">Upload SIM</label>
                        <input type="file" wire:model="sim" name="sim" class="form-control-file">
                      </div>
          @error('sim')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror       
        </div>
        <div class="mb-3">            
            <div class="form-group">                      
                        <label class="form-control-label">Upload SKCK</label>
                        <input type="file" wire:model="skck" name="skck" class="form-control-file">
                    </div>
          @error('skck')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror       
        </div><div class="mb-3">            
            <div class="form-group">                      
              <label class="form-control-label">Upload Domisili</label>
                        <input type="file" wire:model="domisili" name="domisili" class="form-control-file">
                      </div>                    
          @error('domisili')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror       
        </div>   
        <div class="mb-3">
          <select name="know" id="" wire:model="known" class="form-control"  placeholder="mitra">
            <option value="">-Mengetahui MyRoket Dari-</option>
            @foreach(known() as $k => $v)
              <option wire:key="{{$k}}" value="{{$k}}">{{$v}}</option>
            @endforeach
          </select>  

          @error('known')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror       
        </div>         
        <div class="row">          
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Register</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <p class="mt-2">
        <a href="{{route('auth.login')}}">Already have an Account?</a>
      </p>      

    </div>
    <!-- /.card-body -->
  </div>
  </div>
  <!-- /.card -->
