@extends('layout.auth-layout')
@section('title','Myrocket | Reset Password')
@section('content')

  <div class="login-logo">
    <a href="{{route('auth.forgot-save')}}"><img class="mr-1" width="50" src="{{url('images/logo.png')}}" alt=""><strong>MyRoket</strong> by JOPAR</a>
  </div>
  <!-- /.login-logo -->
  <div class="card card-outline card-primary">    
    <div class="card-body">
      <p class="login-box-msg">Reset your password</p>

      <form action="{{route('auth.reset')}}" method="POST">
        @csrf
        @if(session()->has('error'))
          <div class="text-danger text-sm my-1"><span>{{session('error')}}</span></div>
        @endif
        <div class="mb-3">
          <input type="password" name="password" class="form-control" placeholder="Password">       
          @error('password')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror              
        </div>

        <div class="mb-3">
          <input type="password" name="password_confirmation" class="form-control" placeholder="Password Confirmation">       
          @error('password_confirmation')
            <div class="text-danger text-sm my-1"><span>{{$message}}</span></div>
          @enderror              
        </div>    

        <div class="row">          
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Reset Now</button>
            <input type="hidden" name="token" value="{{request('token')}}">
          </div>
          <!-- /.col -->
        </div>
      </form>
          
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
@endsection