<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@if (isset($title))
    {{$title}}
    @else
    @yield('title','Myroket')
    @endif
</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/adminlte.css')}}">
  <link rel="shortcut icon" type="image/png" href="{{asset('favicon.png')}}"/>
  @livewireStyles
</head>
<body class="hold-transition {{!request()->is('auth/register')?'login-page':'register-page'}}">
<div class="login-box">
  @if (isset($slot))
    {{$slot}}
    @else
    @yield('content')
    @endif    
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
@stack('custom-scripts')
<script>
  document.addEventListener('livewire:load', function(e) {
        window.livewire.on('showAlert', (data) => {
            toastr.success(data.msg, 'Success!!', {
              timeOut: 5000,
              closeButton: !0,
              debug: !1,
              newestOnTop: !0,
              progressBar: !0,
              positionClass: "toast-top-right",
              preventDuplicates: !0,
              onclick: null,
              showDuration: "300",
              hideDuration: "1000",
              extendedTimeOut: "1000",
              showEasing: "swing",
              hideEasing: "linear",
              showMethod: "fadeIn",
              hideMethod: "fadeOut",
              tapToDismiss: !1
            })
        });
        
        
    })

</script>
@livewireScripts
</body>
</html>
