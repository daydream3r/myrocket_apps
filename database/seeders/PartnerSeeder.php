<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Partner;

class PartnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $partners = ['hub','pos','motor','mobil','pindahan','toko','bisnis'];
        foreach($partners as $partner){
            Partner::create([
                'name'=>$partner
            ]);    
        }
        
    }
}
