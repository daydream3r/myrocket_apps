<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Permission;
use App\Models\Menu;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Menu::create(['name'=>'dashboard','parent_id'=>null,'slug'=>'dashboard','permission_id'=>$this->findPermission('view dashboard'),'position'=>0]);
        Menu::create(['name'=>'pengaturan','parent_id'=>null,'slug'=>'#','permission_id'=>$this->findPermission('view setting'),'position'=>9]);
        Menu::create(['name'=>'user','parent_id'=>$this->findParent('pengaturan'),'slug'=>'user','permission_id'=>$this->findPermission('view user'),'position'=>2]);
        Menu::create(['name'=>'mitra','parent_id'=>null,'slug'=>'partner','permission_id'=>$this->findPermission('view partner'),'position'=>1]);
        Menu::create(['name'=>'role','parent_id'=>$this->findParent('pengaturan'),'slug'=>'role','permission_id'=>$this->findPermission('view role'),'position'=>4]);                
        Menu::create(['name'=>'profile pengguna','parent_id'=>$this->findParent('pengaturan'),'slug'=>'profile','permission_id'=>$this->findPermission('view profile'),'position'=>0]);
        Menu::create(['name'=>'ganti password','parent_id'=>$this->findParent('pengaturan'),'slug'=>'change-password','permission_id'=>$this->findPermission('view profile'),'position'=>1]);
        Menu::create(['name'=>'menu','parent_id'=>$this->findParent('pengaturan'),'slug'=>'menu','permission_id'=>$this->findPermission('view menu'),'position'=>3]);
        Menu::create(['name'=>'laporan','parent_id'=>null,'slug'=>'#','permission_id'=>$this->findPermission('view report'),'position'=>3]);
        Menu::create(['name'=>'Roket Cepat','parent_id'=>$this->findParent('laporan'),'slug'=>'laporan/roket-cepat','permission_id'=>$this->findPermission('view report'),'position'=>0]);
        Menu::create(['name'=>'Roket Pindahan','parent_id'=>$this->findParent('laporan'),'slug'=>'laporan/roket-pindahan','permission_id'=>$this->findPermission('view report'),'position'=>1]);
        Menu::create(['name'=>'Roket Kargo','parent_id'=>$this->findParent('laporan'),'slug'=>'laporan/roket-kargo','permission_id'=>$this->findPermission('view report'),'position'=>2]);
        Menu::create(['name'=>'transaksi','parent_id'=>null,'slug'=>'#','permission_id'=>$this->findPermission('view transaction'),'position'=>8]);
        Menu::create(['name'=>'roket cepat','parent_id'=>$this->findParent('transaksi'),'slug'=>'transaction/roket-cepat','permission_id'=>$this->findPermission('view transaction'),'position'=>0]);
        Menu::create(['name'=>'roket pindahan','parent_id'=>$this->findParent('transaksi'),'slug'=>'transaction/roket-pindahan','permission_id'=>$this->findPermission('view transaction'),'position'=>1]);
        Menu::create(['name'=>'roket esok','parent_id'=>$this->findParent('transaksi'),'slug'=>'transaction/roket-esok','permission_id'=>$this->findPermission('view transaction'),'position'=>2]);
        Menu::create(['name'=>'roket regular','parent_id'=>$this->findParent('transaksi'),'slug'=>'transaction/roket-regular','permission_id'=>$this->findPermission('view transaction'),'position'=>3]);
        Menu::create(['name'=>'roket kilat','parent_id'=>$this->findParent('transaksi'),'slug'=>'transaction/roket-kilat','permission_id'=>$this->findPermission('view transaction'),'position'=>4]);
        Menu::create(['name'=>'roket darat','parent_id'=>$this->findParent('transaksi'),'slug'=>'transaction/roket-darat','permission_id'=>$this->findPermission('view transaction'),'position'=>5]);
        Menu::create(['name'=>'roket plus','parent_id'=>$this->findParent('transaksi'),'slug'=>'transaction/roket-plus','permission_id'=>$this->findPermission('view transaction'),'position'=>6]);
        Menu::create(['name'=>'administrator','parent_id'=>null,'slug'=>'#','permission_id'=>$this->findPermission('view admin'),'position'=>10]);
        Menu::create(['name'=>'user','parent_id'=>$this->findParent('administrator'),'slug'=>'user','permission_id'=>$this->findPermission('view user'),'position'=>0]);
        Menu::create(['name'=>'role','parent_id'=>$this->findParent('administrator'),'slug'=>'role','permission_id'=>$this->findPermission('view role'),'position'=>1]);
        Menu::create(['name'=>'permission','parent_id'=>$this->findParent('administrator'),'slug'=>'permission','permission_id'=>$this->findPermission('view permission'),'position'=>2]);

    }

    protected function findPermission($name){
        $permission = Permission::whereName($name)->first();
        return ($permission?$permission->id:null);
    }

    protected function findParent($name){

        return (Menu::whereName($name)->first()?Menu::whereName($name)->first()->id:null);
    }
}
