<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name'=>'adminku',
            'email'=>'adminku@gmail.com',
            'email_verified_at'=>now(),
            'password'=>bcrypt('password'),
            'status'=>1,
        ]);   

        $user = User::create([
            'name'=>'day',
            'email'=>'daydream3r91@gmail.com',
            'email_verified_at'=>now(),
            'password'=>bcrypt('password'),
            'status'=>1,
        ]);   

        $permission_to_user = ['view dashboard','view profile','view partner','create partner','update partner','view transaction'];

        $role = Role::create(['name' => 'admin']);
        $admin->assignRole([$role->id]);
        $role->givePermissionTo(Permission::all());

        $role = Role::create(['name' => 'user'])->givePermissionTo($permission_to_user);
        $user->assignRole([$role->id]);


        
    }
}
