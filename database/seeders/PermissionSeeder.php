<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Permission;


class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'view user']);
        Permission::create(['name' => 'create user']);
        Permission::create(['name' => 'update user']);        
        Permission::create(['name' => 'delete user']);   
        Permission::create(['name' => 'verify user']);        

        Permission::create(['name' => 'view menu']);
        Permission::create(['name' => 'delete menu']);

        Permission::create(['name' => 'create permission']);
        Permission::create(['name' => 'view permission']);
        Permission::create(['name' => 'update permission']);
        Permission::create(['name' => 'delete permission']);

        Permission::create(['name' => 'view role']);
        Permission::create(['name' => 'create role']);
        Permission::create(['name' => 'update role']);
        Permission::create(['name' => 'delete role']);

        Permission::create(['name' => 'view partner']);
        Permission::create(['name' => 'create partner']);
        Permission::create(['name' => 'update partner']);
        Permission::create(['name' => 'delete partner']);

        Permission::create(['name' => 'create transaction']);
        Permission::create(['name' => 'update transaction']);
        Permission::create(['name' => 'view transaction']);
        Permission::create(['name' => 'delete transaction']);

        Permission::create(['name' => 'view setting']);
        Permission::create(['name' => 'view dashboard']);

        Permission::create(['name' => 'view profile']);
        Permission::create(['name' => 'view report']);
        Permission::create(['name' => 'view admin']);

    }
}
